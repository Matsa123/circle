
window.addEventListener("DOMContentLoaded", (e) => {
    //находим  главную кнопку
    const btn = document.querySelector('.btn');

    //создаем переменную, которая создаст контейнер, вешаем класс на него
    const father = document.createElement("div");
    father.classList.add("father");

    //создаем переменную, которая создаст поле ввода
    const input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "Введите диаметр круга");
    input.setAttribute("value", "");

    //создаем переменную, которая создаст кнопку "Нарисовать"
    const submit = document.createElement("input");
    submit.setAttribute("type", "submit");
    submit.setAttribute("value", "Нарисовать");

    //клик по главной кнопке, удаляем ее, создаем поле ввода и кнопку подтвердить
    btn.addEventListener("click", () => {
        btn.remove();
        document.body.appendChild(father);
        father.append(input);
        father.append(submit);
    }, false);

    //клик по кнопке "Нарисовать", берем введеные значения и удаляем инпуты
    submit.addEventListener("click", () => {
        const d = input.value + "px";
        input.remove();
        submit.remove();

        //создаем 100 кругов рандомного цвета диаметром, который задал пользователь 
        for (let i = 0; i < 100; i++) {
            const a = document.createElement("div");
            father.appendChild(a);
            a.classList.add('cir100');
            a.style.width = d;
            a.style.height = d;

            //создаем рандомный цвет
            let r = Math.floor(Math.random() * (256));
            let g = Math.floor(Math.random() * (256));
            let b = Math.floor(Math.random() * (256));
            let color = '#' + r.toString(16) + g.toString(16) + b.toString(16);
            a.style.backgroundColor = color;
        };

        //клик удаления выбранного круга
        father.addEventListener("click", (e) => {
            if (e.target == father) {
                return;
            } else {
                e.target.remove();
            };
        }, false);


    }, false);
}, false);

